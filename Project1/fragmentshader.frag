#version 430 core

// Output color
out vec4 fragColor;
in vec4 vColor;

void main()
{
    fragColor = vColor;
}
