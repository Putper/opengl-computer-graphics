#include <iostream>

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "glsl.h"


using namespace std;

GLuint program_id;

/// <summary>
/// Height & width of screen
/// </summary>
const int WIDTH = 800, HEIGHT = 600;
const char* vertexshader_name = "vertexshader.vert";
const char* fragshader_name = "fragmentshader.frag";

/// <summary>
/// keyboard handling
/// </summary>
/// <param name="key"></param>
/// <param name="a"></param>
/// <param name="b"></param>
void keyboardHandler(unsigned char key, int a, int b)
{
    if (key == 27)
        glutExit();
}


/// <summary>
/// Rendering
/// </summary>
void Render()
{
    // background
    static const GLfloat green[] = { 0.0, 0.25, 0.1, 1.0 };
    glClearBufferfv(GL_COLOR, 0, green);

    // triangle
    //glBegin(GL_TRIANGLES);
    //glColor3f(0.0, 1.0, 0.0); // green
    //glVertex2f(-0.5, -0.5);
    //glColor3f(1.0, 0.0, 0.0); // red
    //glVertex2f(0.5, -0.5);
    //glColor3f(0.0, 0.0, 1.0); // blue
    //glVertex2f(0, 0.5);
    //glEnd();

    // square
    //glBegin(GL_QUADS);
    //glColor3f(0.0, 1.0, 0.0); // green
    //glVertex2f(-0.5, 0.5);
    //glColor3f(1.0, 1.0, 0.0); // yellow
    //glVertex2f(-0.5, -0.5);
    //glColor3f(1.0, 0.0, 0.0); // red
    //glVertex2f(0.5, -0.5);
    //glColor3f(0.0, 0.0, 1.0); // blue
    //glVertex2f(0.5, 0.5);
    //glEnd();

    glUseProgram(program_id);

    glPointSize(40.0f);
    glDrawArrays(GL_POINTS, 0, 1);

    glutSwapBuffers();
}


/// <summary>
/// Initializes Glut and Glew
/// </summary>
/// <param name="argc"></param>
/// <param name="argv"></param>
void InitGlutGlew(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize(WIDTH, HEIGHT);
    glutCreateWindow("Hello OpenGL");
    glutDisplayFunc(Render);
    glutKeyboardFunc(keyboardHandler);

    glewInit();
}

/// <summary>
/// initialise shaders
/// </summary>
void InitShaders()
{
    char* vertexshader = glsl::readFile(vertexshader_name);
    GLuint vsh_id = glsl::makeVertexShader(vertexshader);

    char* fragshader = glsl::readFile(fragshader_name);
    GLuint fsh_id = glsl::makeFragmentShader(fragshader);

    program_id = glsl::makeShaderProgram(vsh_id, fsh_id);
}

void InitGLM()
{
    // Create vector
    glm::vec4 position = glm::vec4(0.3f, 0.4f, 0.0f, 1.0f);
    glm::vec4 colour = glm::vec4(1.0f, 1.0f, 0.0f, 1.0f);

    // Get location of uniform variable (create if new)
    GLuint position_id = glGetUniformLocation(program_id, "position");
    GLuint color_id = glGetUniformLocation(program_id, "color");
    
    // Attach to program (needed to fill uniform vars)
    glUseProgram(program_id);

    // Specify the value of the uniform variable
    // Args: location, count, value
    glUniform4fv(position_id, 1, glm::value_ptr(position));
    glUniform4fv(color_id, 1, glm::value_ptr(colour));
}


int main(int argc, char** argv)
{
    InitGlutGlew(argc, argv);

    // Hide console window
    HWND hWnd = GetConsoleWindow();
    ShowWindow(hWnd, SW_HIDE);

    InitShaders();
    InitGLM();

    // Main loop
    glutMainLoop();

    return 0;
}
